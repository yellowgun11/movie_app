import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'dart:io';
import 'dart:convert';

class CommunityPage extends StatefulWidget {
  @override
  CommunityPageState createState() => new CommunityPageState();
}

class CommunityPageState extends State<CommunityPage> {
  List swiperDataList = [
    "https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2591947273.webp",
    "https://img9.doubanio.com/view/photo/s_ratio_poster/public/p2504791226.webp",
    "https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2543785440.webp",
  ];
  List data;

  @override
  void initState() {
    super.initState();
    print('initState');
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: CustomScrollView(
        slivers: <Widget>[
          //AppBar，包含一个导航栏
          SliverAppBar(
            leading: Builder(builder: (context) {
              return IconButton(
                icon: Icon(Icons.notifications, size: 30, color: Colors.grey),
                onPressed: () {},
              );
            }),
            actions: <Widget>[
              // 导航右侧菜单
              IconButton(
                icon: Icon(
                  Icons.search,
                  size: 30,
                  color: Colors.grey,
                ),
                onPressed: () {},
              ),
            ],
            title: new Text('首页'),
            centerTitle: true,
            pinned: true,
            expandedHeight: 0,
            // flexibleSpace: FlexibleSpaceBar(
            //   // title: const Text('Demo'),
            //   background: Image.asset(
            //     "./images/user0.jpg",
            //     fit: BoxFit.cover,
            //   ),
            // ),
          ),

          new SliverToBoxAdapter(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 200.0,
              child: Swiper(
                itemBuilder: _swiperBuilder,
                itemCount: 3,
                pagination: new SwiperPagination(
                    builder: DotSwiperPaginationBuilder(
                  color: Colors.black54,
                  activeColor: Colors.white,
                )),
                control: new SwiperControl(),
                scrollDirection: Axis.horizontal,
                autoplay: true,
                onTap: (index) => print('点击了第$index个'),
              ),
            ),
          ),
          //List
          new SliverFixedExtentList(
            itemExtent: 100.0,
            delegate: new SliverChildBuilderDelegate(
                (BuildContext context, int index) {
              //创建列表项
              return new Card(
                child: new Container(
                  padding: new EdgeInsets.all(10.0),
                  child: new ListTile(
                    // leading 最左侧头部
                    leading: new Icon(Icons.cake),
                    // subtitle 富文本标题
                    subtitle: new Container(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("姓名："),
                              new Expanded(
                                child: new Text(
                                  data[index]["title"],
                                  softWrap: false,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0),
                                ),
                              )
                            ],
                          ),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Text("时间: "),
                                  new Text("2012-12-12 12:12"),
                                ],
                              )
                            ],
                          ),
                          new Row(
                            children: <Widget>[
                              new Container(
                                padding: const EdgeInsets.fromLTRB(
                                    0.0, 8.0, 0.0, 2.0),
                                child: new Text(
                                    'id: ' + data[index]["id"].toString()),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    // trailing 展示在title后面最末尾的后缀组件
                    trailing: new Icon(
                      Icons.keyboard_arrow_right,
                      color: Colors.grey,
                    ), // 显示右侧的剪头,不显示则传null
                    onTap: () {},
                  ),
                ),
              );
            }, childCount: data == null ? 0 : data.length 
                ),
          ),
        ],
      ),
    );
  }

  Widget _swiperBuilder(BuildContext context, int index) {
    return (Image.network(
      swiperDataList[index],
      fit: BoxFit.fill,
    ));
  }

  getData() async {
    var url = "http://jsonplaceholder.typicode.com/posts";
    var httpClient = new HttpClient();
    var result;
    try {
      var request = await httpClient.getUrl(Uri.parse(url));
      var response = await request.close();
      if (response.statusCode == HttpStatus.ok) {
        var json = await response.transform(utf8.decoder).join();
        result = jsonDecode(json);
      } else {
        result =
            "error getting json data:\n http status ${response.statusCode}";
      }
    } catch (exception) {
      result = "fail getting json data";
    }
    if (!mounted) return;

    print(result);
    setState(() {
      data = result;
    });
  }
}
