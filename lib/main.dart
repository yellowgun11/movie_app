import 'package:flutter/material.dart';
import './page/home.dart';
import './page/community.dart';
import './page/order.dart';
import './page/user.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, //去掉调试
      title: 'Movie App',
      theme: ThemeData(
        // primarySwatch: Colors.amber,
        primaryColor:Colors.white
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {
  TabController controller; // TabController就是tabbar的控制器

  @override
  void initState() {
    controller = new TabController(length: 4, vsync: this); // length表示有几个tab
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new TabBarView(controller: controller, children: <Widget>[
        new HomePage(),
        new CommunityPage(),
        new OrderPage(),
        new UserPage()
      ]),
      // bottomNavigationBar底部导航栏
      bottomNavigationBar: new Material(
          color: Colors.white,
          child: new TabBar(
            controller: controller,
            tabs: <Widget>[
              new Tab(icon:new Icon(Icons.home,size: 35)),
              new Tab(icon:new Icon(Icons.chat,size: 35)),
              new Tab(icon:new Icon(Icons.assignment,size: 35)),
              new Tab(icon:new Icon(Icons.person,size: 35)),
            ],
            indicatorColor: Colors.orange,  //tab标签的下划线颜色
          ),
      ),
    );
  }
}


// Widget build(BuildContext context) {
//     return new Scaffold(
//       body: new TabBarView(controller: controller, children: <Widget>[
//         new HomePage(),
//         new CommunityPage(),
//         new OrderPage(),
//         new UserPage()
//       ]),
//       // bottomNavigationBar底部导航栏
//       bottomNavigationBar: new Material(
//           color: Colors.white,
//           child: new TabBar(
//             controller: controller,
//             tabs: <Widget>[
//               new Tab(icon:new Icon(Icons.home)),
//               new Tab(icon:new Icon(Icons.chat)),
//               new Tab(icon:new Icon(Icons.list)),
//               new Tab(icon:new Icon(Icons.person)),
//             ],
//             indicatorColor: Colors.orange,  //tab标签的下划线颜色
//           ),
//       ),
//     );
//   }